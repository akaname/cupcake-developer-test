//
// KEYBOAR MANAGER
//

function Keyboard(code) {
    let key = {};
    key.code = code;
    key.dowm = false;
    key.up = true;
    key.pressing = undefined;
    key.pressed = undefined;
    key.release = undefined;

    key.downHandler = event => {
        if (event.keyCode === key.code) {
            if (key.up && key.pressed) {
                key.pressed();
            } else if (key.pressing) {
                key.pressing();
            }
            key.dowm = true;
            key.up = false;
        }
        event.preventDefault();
    };

    key.upHandler = event => {
        if (event.keyCode === key.code) {
            if (key.dowm && key.release) key.release();
            key.dowm = false;
            key.up = true;
        }
        event.preventDefault();
    };

    key.unregister = function () {
        key.pressing = undefined;
        key.pressed = undefined;
        key.release = undefined;
    };

    //Attach event listeners
    window.addEventListener(
        "keydown", key.downHandler.bind(key), false
    );
    window.addEventListener(
        "keyup", key.upHandler.bind(key), false
    );

    return key;
}

//
//KEY TYPES
//

Keys.SET = {
    WASD: 0,
    ARROWS: 1,
};

//
//KEY SPRITE
//
Keys.WASD_SPRITE = 'wasd';
Keys.ARROW_SPRITE = 'arrows';

//
//BASIC PLAYER INPUT
//

function Keys(keySet) {
    this.keySet = keySet;
    this.up = null;
    this.down = null;
    this.left = null;
    this.right = null;

    if (this.keySet === Keys.SET.WASD) {
        this.toWASD();
    } else if (this.keySet === Keys.SET.ARROWS) {
        this.toArrows();
    }
}

Keys.prototype.unregister = function () {
    this.up.unregister();
    this.down.unregister();
    this.left.unregister();
    this.right.unregister();
};

Keys.prototype.toWASD = function () {
    this.up = new Keyboard(87);
    this.down = new Keyboard(83);
    this.left = new Keyboard(65);
    this.right = new Keyboard(68);
};

Keys.prototype.toArrows = function () {
    this.up = new Keyboard(38);
    this.down = new Keyboard(40);
    this.left = new Keyboard(37);
    this.right = new Keyboard(39);
};

//
//BASIC CONFIRMATION FOR POPUPS
//

function Confirm() {
    this.confirm = new Keyboard(13);
}

Confirm.prototype.unregister = function () {
    this.confirm.unregister();
};
