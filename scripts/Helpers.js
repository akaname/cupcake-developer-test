//
// HELP FUNCTIONS
//

function Vector2(x, y) {
    this.x = x;
    this.y = y;
};

Math.lerp = function (value1, value2, amount) {
    amount = amount < 0 ? 0 : amount;
    amount = amount > 1 ? 1 : amount;
    return value1 + (value2 - value1) * amount;
};

Directions = {
    UP: 'up',
    DOWN: 'down',
    LEFT: 'left',
    RIGHT: 'right'
}

Audio.CAT_MOVE = 'cat-move';
Audio.CAT_WIN = 'cat-win';
Audio.CAT_LOOSE = 'cat-loose';
Audio.DOG_MOVE = 'dog-move';
Audio.DOG_WIN = 'dog-win';
Audio.DOG_LOOSE = 'dog-loose';
