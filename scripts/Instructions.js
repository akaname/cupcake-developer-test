//
// Popup showing, just before the game starts,  the objective and the rules of the game
//

function Instructions(screen, ticker, continueCallback) {

    var text =
        'OBJECTIVE:\n\n' +
        'leave the enemy with no moves!\n\n\n\n' +
        '   RULES: \n \n' +
        '# you have to move every turn \n \n' +
        '# You can\'t walk over a tile you\'ve been in \n \n' +
        '# Tiles walked over by both players gets destroyed \n \n \n \n';

    this.instructions = new View(screen, ticker, continueCallback, text, 700, 400);
}

Instructions.prototype.initialize = function (stage) {
    this.instructions.initialize(stage, true);
};
