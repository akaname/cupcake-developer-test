//
// Basic popup that show information and continues after pressing <enter>
//

function View(screen, ticker, continueCallback, text, sizeX, sizeY) {
    this.screenWidth = screen.width;
    this.screenHeight = screen.height;
    this.ticker = ticker;
    this.continueCallback = continueCallback;
    this.text = text;
    this.alphaValue = 1;
    this.toOne = false;
    this.viewArea = new PIXI.Container();
    this.confirmation = new Confirm();

    this.font = new PIXI.extras.BitmapText(this.text, {
        font: "16px font",
        align: "center",
    });
    this.font.anchor.set(0.5, 0.5);

    this.confirmationFont = new PIXI.extras.BitmapText('Press [ENTER] to play!', {
        font: "16px font",
        align: "center",
    });
    this.confirmationFont.anchor.set(0.5, 0.5);

    this.background = new PIXI.Graphics();
    this.background.lineStyle(10, 0x0a0a0a, 1);
    this.background.beginFill(0x151515, 1);
    this.background.drawRect(0, 0, sizeX, sizeY);
}

// initializes and sets visibility
View.prototype.initialize = function (stage, visibility) {
    stage.addChild(this.viewArea);

    this.background.pivot.x = this.background.width * 0.5;
    this.background.pivot.y = this.background.height * 0.5;

    this.viewArea.addChild(this.background);
    this.viewArea.addChild(this.font);
    this.viewArea.addChild(this.confirmationFont);

    this.confirmationFont.position.y = this.background.height * 0.3125;

    this.viewArea.position.x = this.screenWidth * 0.5;
    this.viewArea.position.y = this.screenHeight * 0.5;

    this.ticker.add(this.blink, this);

    this.viewArea.visible = visibility;
    if (visibility) {
        this.confirmation.confirm.pressed = () => {
            this.hide();
        };
    }
};

// updates the text of the view
View.prototype.updateView = function (text, size) {
    var font = size + 'px font';

    this.viewArea.removeChild(this.font);

    this.font = new PIXI.extras.BitmapText(text, {
        font: font,
        align: "center",
    });
    this.font.anchor.set(0.5, 0.5);

    this.viewArea.addChild(this.font);

    this.viewArea.visible = true;

    this.confirmation.confirm.pressed = () => {
        this.hide();
    };
};

// hides pop, starting leaveScreen function
View.prototype.hide = function () {
    this.ticker.remove(this.blink, this);

    this.confirmationFont.alpha = 1;
    this.confirmation.unregister();

    this.ticker.add(this.leaveScreen, this);
};

// leaves the screen then continues
View.prototype.leaveScreen = function (delta) {
    this.viewArea.position.x += delta * 10;

    if (this.viewArea.x > this.screenWidth * 2) {
        this.ticker.remove(this.leaveScreen, this);
        if (this.continueCallback) {
            this.continueCallback();
        }
    }
};

// blink animation
View.prototype.blink = function (delta) {
    var amount = delta * 0.045;
    var newAlpha = this.toOne ? (this.alphaValue + amount) : (this.alphaValue - amount);
    this.alphaValue = Math.lerp(this.alphaValue, newAlpha, 0.5);

    this.confirmationFont.alpha = this.alphaValue;

    if (this.alphaValue >= 1) {
        this.toOne = false;
    } else if (this.alphaValue <= 0.4) {
        this.toOne = true;
    }
};
