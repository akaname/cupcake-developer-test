//
// Main handles the application
//

function Main() {
    // pixi application
    this.app = new PIXI.Application(800, 600);

    // game stage
    this.stage = new PIXI.Container();

    // game logic
    this.game;

    // game renderer
    this.renderer;
    // game start menu
    this.startMenu;

    // creating renderer
    this.createRenderer();

    // loading assets async
    this.loadAssets();
}

// start game after assets are loaded
Main.prototype.start = function (gameAudio, startMenuAudio) {
    this.game = new Game(this.app, gameAudio, this.resetGame.bind(this));
    this.startMenu = new StartMenu(this.app.screen, this.app.ticker);
    this.startMenu.initialize(this.stage, startMenuAudio, this.createNewGame.bind(this));
    this.app.ticker.add(this.update.bind(this));
};

// updates game and renderer
Main.prototype.update = function (delta) {
    if (this.game) {
        this.game.update(delta);
    }
    this.renderer.render(this.stage);
};

// loads assets async
Main.prototype.loadAssets = function () {
    var assets = 'assets/';
    var textures = 'textures/';
    var fonts = 'fonts/';
    var audio = 'audio/';

    var loader = PIXI.loader;

    // textures
    loader.add(Tile.SPRITE_TAG, assets + textures + 'tile.png');
    loader.add(Player.CAT_TAG, assets + textures + 'cat.png');
    loader.add(Player.DOG_TAG, assets + textures + 'dog.png');
    loader.add(Player.CAT_PAW_TAG, assets + textures + 'cat-paws.png');
    loader.add(Player.DOG_PAW_TAG, assets + textures + 'dog-paws.png');
    loader.add(Keys.ARROW_SPRITE, assets + textures + 'arrows.png');
    loader.add(Keys.WASD_SPRITE, assets + textures + 'wasd.png');

    //fonts
    loader.add('font', assets + fonts + 'font.fnt');

    //audios
    loader.add(StartMenu.AUDIO_FLIP, assets + audio + 'flip.mp3');
    loader.add(StartMenu.AUDIO_MEOW, assets + audio + 'meow.mp3');
    loader.add(StartMenu.AUDIO_BARK, assets + audio + 'bark.mp3');
    loader.add(Audio.CAT_MOVE, assets + audio + 'cat-move.mp3');
    loader.add(Audio.CAT_WIN, assets + audio + 'cat-win.mp3');
    loader.add(Audio.CAT_LOOSE, assets + audio + 'cat-loose.mp3');
    loader.add(Audio.DOG_MOVE, assets + audio + 'dog-move.mp3');
    loader.add(Audio.DOG_WIN, assets + audio + 'dog-win.mp3');
    loader.add(Audio.DOG_LOOSE, assets + audio + 'dog-loose.mp3');

    loader.on('progress', this.onLoadProgress, this);
    loader.on('error', this.onLoadError, this);
    loader.once('complete', this.onLoadFinished.bind(this));
    loader.load();
};

// called once per loaded/errored file
Main.prototype.onLoadProgress = function (loader, resources) {
    console.log(resources.name, 'loadded. ', 'Progress: ', resources.progressChunk, '%');
};

// called once per errored file
Main.prototype.onLoadError = function (err, loader, resource) {
    console.log(err, ' at ', resource.name);
};

// called once when the queued resources all load.
Main.prototype.onLoadFinished = function (loader, resources) {

    var startMenuAudio = {
        flip: resources[StartMenu.AUDIO_FLIP].sound,
        meow: resources[StartMenu.AUDIO_MEOW].sound,
        bark: resources[StartMenu.AUDIO_BARK].sound,

    };

    var gameAudio = {
        catMove: resources[Audio.CAT_MOVE].sound,
        catWin: resources[Audio.CAT_WIN].sound,
        catLoose: resources[Audio.CAT_LOOSE].sound,
        dogMove: resources[Audio.DOG_MOVE].sound,
        dogWin: resources[Audio.DOG_WIN].sound,
        dogLoose: resources[Audio.DOG_LOOSE].sound,

    };

    console.log('Complete loading resources');

    this.start(gameAudio, startMenuAudio);
};

// creation of game renderer
Main.prototype.createRenderer = function () {
    this.renderer = PIXI.autoDetectRenderer(
        800,
        600, {
            view: document.getElementById("game-canvas"),
            transparent: true,
        }
    );

    document.body.appendChild(this.renderer.view);
    this.renderer.view.style.position = 'absolute';
    this.renderer.view.style.left = '50%';
    this.renderer.view.style.top = '50%';
    // -55% instead of -50%, rule of design. Staying a bit off to the left feels better to the eye
    this.renderer.view.style.transform = 'translate3d( -55%, -50%, 0 )';
};

// creates new game
Main.prototype.createNewGame = function (player1, player2) {
    if (this.game) {
        this.game.newGame(this.app, this.stage, player1, player2);
    }
};

// resets game to play again
Main.prototype.resetGame = function () {
    this.startMenu.reset();
    this.game.reset(this.stage, this.startMenu.startAfterReset.bind(this.startMenu));
};
