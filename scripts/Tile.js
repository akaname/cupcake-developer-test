//
// Board tiles
//

Tile.prototype = Object.create(PIXI.Sprite.prototype);
Tile.prototype.constructor = Tile;

// tile states
Tile.STATE = {
    NOT_WALKABLE: {
        id: -1,
        color: 'black',
    },
    WALKABLE: {
        id: 0,
        color: 'white',
    },
    PLAYER_ONE: {
        id: 1,
        color: 'red',
    },
    PLAYER_TWO: {
        id: 2,
        color: 'blue',
    },
};

// tile sprite tag
Tile.SPRITE_TAG = 'tile';

function Tile(x, y) {
    var texture = new PIXI.Texture.fromImage(Tile.SPRITE_TAG);
    PIXI.Sprite.call(this, texture);

    // tile properties
    this.state = 0;
    this.occupied = false;
    this.trail = [];

    // sprite properties
    this.anchor.set(0.5);
    this.position.x = x;
    this.position.y = y;
}

// setter for tile id
Tile.prototype.setID = function (y, x) {
    this.id = new Vector2(x, y);
};

// occupy tile with player
Tile.prototype.occupy = function (id) {
    if (this.state === 0) {
        this.state = id;
    } else {
        this.state = -1;
        this.tint = 0x000000;
        this.alpha = 0;
    }
    this.occupied = true;
};

// unoccupy when player leaves tile
Tile.prototype.unoccupy = function () {
    this.occupied = false;
};

// check id any player has occupied
Tile.prototype.isUntouched = function () {
    if (this.state === 0) {
        return true;
    }

    return false;
};

// check if tile has been destroyed
Tile.prototype.isDestroyed = function () {
    if (this.state === -1) {
        return true;
    }

    return false;
};

// checks if is/was occupied by enemy
Tile.prototype.isEnemy = function (playerId) {
    if (!this.isDestroyed() &&
        this.isUntouched() &&
        this.state !== playerId) {
        return true;
    }

    return false;
}

// checks if tile is walkable
Tile.prototype.isWalkable = function (playerID) {
    if (this.state !== playerID &&
        !this.isDestroyed()) {
        return true;
    }

    return false;
};

// checks if tile is equal to another
Tile.prototype.equals = function (tile) {
    if (this.id.x === tile.id.x &&
        this.id.y === tile.id.y) {
        return true;
    }

    return false;
};

// adds paw trail to the board
Tile.prototype.addTrail = function (spriteTag, direction) {
    var newTrail = new PIXI.Sprite.fromImage(spriteTag);

    var rotation = 0;

    if (direction === Directions.DOWN) {
        rotation = Math.PI * 2 * 0.5;
    } else if (direction === Directions.LEFT) {
        rotation = Math.PI * 2 * 0.75;
    }
    if (direction === Directions.RIGHT) {
        rotation = Math.PI * 2 * 0.25;
    }

    if (newTrail) {
        newTrail.width = this.width * 0.7;
        newTrail.height = this.height * 0.7;
        newTrail.anchor.set(0.5, 0.5);
        newTrail.rotation = rotation;
        newTrail.position.x = this.position.x;
        newTrail.position.y = this.position.y;
        this.trail.push(newTrail);
        this.parent.addChild(newTrail);
    }
};

// removes trail
Tile.prototype.removeTrail = function () {
    for (var i = 0; i < this.trail.length; i++) {
        this.parent.removeChild(this.trail[i]);
    }

    this.trail = [];
}
