//
// Handles the game logic
//

Game.STATE = {
    STARTING: 0,
    GAME: 1,
    END: 2,
}

function Game(app, audio, playAgainCallback) {
    // game state
    this.state;

    //game audio
    this.audio = audio;

    // game turns
    this.turn;

    // creation of board
    this.board = new Board(app.screen);

    // game instructions
    this.instructions = new Instructions(app.screen, app.ticker, this.startGame.bind(this));

    // game end
    this.ending = new Ending(app.screen, app.ticker);

    // game callback to play again
    this.playAgain = playAgainCallback;
}

Game.prototype.newGame = function (app, stage, player1, player2) {
    //setting game initial state
    this.state = Game.STATE.STARTING;

    //build board
    this.board.buildBoard(5, 5, stage);

    var player_one = this.getPlayerFromType(1, player1);
    player_one.setAudio(this.audio.catMove, this.audio.catWin, this.audio.catLoose);

    var player_two = this.getPlayerFromType(2, player2);
    player_two.setAudio(this.audio.dogMove, this.audio.dogWin, this.audio.dogLoose);

    //sets players positions
    this.board.initializePlayers(player_one, player_two);

    //adding players to stage
    stage.addChild(player_one, player_two);

    //controlls turn bahavior
    this.turn = new Turn(app.ticker, this.board, [player_one, player_two], this.finishGame.bind(this));

    //show instructions
    this.instructions.initialize(stage);

    //shows winner
    this.ending.initialize(stage, false);
};

Game.prototype.startGame = function () {
    this.turn.play();
    this.state = Game.STATE.GAME;
};

Game.prototype.update = function (delta) {
    if (this.state === Game.STATE.GAME) {
        this.turn.update();
        this.turn.animations(delta);
    }
};

Game.prototype.finishGame = function (player) {
    this.state = Game.STATE.END;
    this.ending.setText(player.team);
    this.playAgain();
};

Game.prototype.reset = function (stage, after) {
    this.ending.view.continueCallback = after;
    this.ending.show();
    this.board.reset();
    this.turn.reset(stage);
};

Game.prototype.getPlayerFromType = function (id, type) {
    var name = id === 1 ? 'Cat' : 'Dog';
    var sprite = id === 1 ? Player.CAT_TAG : Player.DOG_TAG;
    var trail = id === 1 ? Player.CAT_PAW_TAG : Player.DOG_PAW_TAG;

    if (type === SelectPlayer.TYPE.HUMAN) {
        var keySet = id === 1 ? Keys.SET.WASD : Keys.SET.ARROWS;
        return new Human(id, sprite, trail, name, keySet);

    } else if (type === SelectPlayer.TYPE.PC_EASY) {
        return new AI_Easy(id, sprite, trail, name);

    } else if (type === SelectPlayer.TYPE.PC_MED) {
        return new AI_Medium(id, sprite, trail, name);

    } else if (type === SelectPlayer.TYPE.PC_HARD) {
        return new AI(id, sprite, trail, name);
    }

    return null;
};
