//
// After game has ended, popup showing winner
//

function Ending(screen, ticker, continueCallback) {
    this.text = 'winner' + '\n\nWINS!';
    this.view = new View(screen, ticker, continueCallback, this.text, screen.width * 0.9, screen.height * 0.9);
}

Ending.prototype.initialize = function (stage, visibility) {
    this.view.initialize(stage, visibility);
};

Ending.prototype.setText = function (text) {
    this.text = text + '\n\nWINS!';
};

Ending.prototype.show = function () {
    this.view.updateView(this.text, 38);
};
