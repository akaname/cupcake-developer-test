//
// Handles human players
//

Human.prototype = Object.create(Player.prototype);
Human.prototype.constructor = Human;

function Human(id, spriteTag, trailTag, name, keySet) {
    Player.call(this, id, spriteTag, trailTag, name);

    // input
    this.keys = new Keys(keySet);
}

// regiter movement
Human.prototype.beginMove = function (turn) {
    this.keys.up.pressed = () => {
        turn.setMove(new Vector2(this.tile.id.x, this.tile.id.y - 1), this.id);
        turn.direction = Directions.UP;
    };

    this.keys.down.pressed = () => {
        turn.setMove(new Vector2(this.tile.id.x, this.tile.id.y + 1), this.id);
        turn.direction = Directions.DOWN;
    };

    this.keys.left.pressed = () => {
        turn.setMove(new Vector2(this.tile.id.x - 1, this.tile.id.y), this.id);
        turn.direction = Directions.LEFT;
    };

    this.keys.right.pressed = () => {
        turn.setMove(new Vector2(this.tile.id.x + 1, this.tile.id.y), this.id);
        turn.direction = Directions.RIGHT;
    };
};

// unregister movement and ends move
Human.prototype.endMove = function () {
    this.keys.unregister();
    this.alpha = 1;
    this.alphaValue = 1;
    this.toOne = false;
};
