//
// Game board
//

Board.MARGIN = 100;
Board.SPACING = 64;

function Board(screen) {
    //INITIALIZATION

    //board size
    var sizeX = 0;
    var sizeY = 0;

    // screen middle point
    var screenMiddleX = screen.width / 2;
    var screenMiddleY = screen.height / 2;

    // board container
    var boardArea = new PIXI.Container();

    // board tiles
    var tiles = [];

    //GETTERS
    this.getXSize = function () {
        return sizeX;
    }
    this.getYSize = function () {
        return sizeY;
    }
    this.getTiles = function () {
        return tiles;
    }
    this.getScreenMiddleX = function () {
        return screenMiddleX;
    }
    this.getScreenMiddleY = function () {
        return screenMiddleY;
    }
    this.getBoardArea = function () {
        return boardArea;
    }

    //SETTERS
    this.setXSize = function (x) {
        sizeX = x;
    }
    this.setYSize = function (y) {
        sizeY = y;
    }
    this.initializeTiles = function () {
        tiles = [];
    }
}

//builds board
Board.prototype.buildBoard = function (x, y, stage) {
    this.setXSize(x);
    this.setYSize(y);
    this.initializeTiles();

    var tileXposition = Board.MARGIN + Board.SPACING / 2;
    var tileYposition = Board.MARGIN + Board.SPACING / 2;

    stage.addChild(this.getBoardArea());

    for (var yIndex = 0; yIndex < this.getYSize(); yIndex++) {
        var tileRow = [];

        for (var xIndex = 0; xIndex < this.getXSize(); xIndex++) {
            var newTile = new Tile(tileXposition, tileYposition);
            newTile.setID(yIndex, xIndex);

            tileRow.push(newTile);

            this.getBoardArea().addChild(newTile);

            tileXposition += Board.SPACING;
        }

        this.getTiles().push(tileRow);

        tileXposition = Board.MARGIN + Board.SPACING / 2;
        tileYposition += Board.SPACING;
    }

    var boardMiddleX = this.getFirst().position.x + (this.getLast().position.x - this.getFirst().position.x) / 2;
    var boardMiddleY = this.getFirst().position.y + (this.getLast().position.y - this.getFirst().position.y) / 2;

    this.getBoardArea().pivot.x = boardMiddleX;
    this.getBoardArea().pivot.y = boardMiddleY;
    this.getBoardArea().x = this.getScreenMiddleX();
    this.getBoardArea().y = this.getScreenMiddleY();
    this.getBoardArea().updateTransform();
};

// resets board
Board.prototype.reset = function () {
    for (var yIndex = 0; yIndex < this.getYSize(); yIndex++) {
        for (var xIndex = 0; xIndex < this.getXSize(); xIndex++) {
            this.getTiles()[yIndex][xIndex].removeTrail();
            this.getBoardArea().removeChild(this.getTiles()[yIndex][xIndex]);
            this.getTiles()[yIndex][xIndex] = undefined;
        }
    }
};

// initializes players position in board
Board.prototype.initializePlayers = function (p1, p2) {
    var position = Math.floor(Math.random() * 4);

    //left top corner board position
    var p1y = this.getFirst().id.y;
    var p1x = this.getFirst().id.x;
    var p1dir = Directions.RIGHT;

    //right bottom corner board position
    var p2y = this.getLast().id.y;
    var p2x = this.getLast().id.y;
    var p2dir = Directions.LEFT;

    if (position === 1) {
        //left middle board position
        p1y = Math.floor(this.getYSize() / 2);
        p1x = 0;

        //right middle board position
        p2y = Math.floor(this.getYSize() / 2);
        p2x = this.getXSize() - 1;
    } else if (position === 2) {
        //left bottom corner board position
        p1y = Math.floor(this.getYSize() - 1);
        p1x = 0;

        //right top corner board position
        p2y = 0;
        p2x = this.getXSize() - 1;
    }

    p1.setTile(this.getTiles()[p1y][p1x], p1dir, false);
    p2.setTile(this.getTiles()[p2y][p2x], p2dir, false);
};

// checks if move is possible
Board.prototype.isWalkable = function (move, id) {
    if (move.x >= 0 && move.x < this.getXSize() &&
        move.y >= 0 && move.y < this.getYSize()) {
        if (this.getTiles()[move.y][move.x].state != id &&
            this.getTiles()[move.y][move.x].state != -1 &&
            !this.getTiles()[move.y][move.x].occupied) {
            return true;
        }
    }
    return false;
};

// checks if players can move
Board.prototype.hasAvailableMove = function (player) {
    if (this.isWalkable(new Vector2(player.getID().x, player.getID().y - 1), player.id)) {
        return true;
    } else if (this.isWalkable(new Vector2(player.getID().x, player.getID().y + 1), player.id)) {
        return true;
    } else if (this.isWalkable(new Vector2(player.getID().x - 1, player.getID().y), player.id)) {
        return true;
    } else if (this.isWalkable(new Vector2(player.getID().x + 1, player.getID().y), player.id)) {
        return true;
    }

    return false;
};

// checks if array contains tile
Board.prototype.hasTile = function (array, tile) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].equals(tile)) {
            return true;
        }
    }
    return false;
};

// checks if board contains ID
Board.prototype.containsId = function (id) {
    if (id.x >= 0 && id.x < this.getXSize() &&
        id.y >= 0 && id.y < this.getYSize()) {
        return true;
    }
    return false;
};

// get tile neighbors
Board.prototype.getNeighbors = function (tile) {
    var neighbors = [];
    var currentTile = null;

    //UP
    if (this.containsId(new Vector2(tile.id.x, tile.id.y - 1))) {
        currentTile = this.getTiles()[tile.id.y - 1][tile.id.x];
        neighbors.push(currentTile);
    }

    //DOWN
    if (this.containsId(new Vector2(tile.id.x, tile.id.y + 1))) {
        currentTile = this.getTiles()[tile.id.y + 1][tile.id.x];
        neighbors.push(currentTile);
    }

    //LEFT
    if (this.containsId(new Vector2(tile.id.x - 1, tile.id.y))) {
        currentTile = this.getTiles()[tile.id.y][tile.id.x - 1];
        neighbors.push(currentTile);
    }

    //RIGHT
    if (this.containsId(new Vector2(tile.id.x + 1, tile.id.y))) {
        currentTile = this.getTiles()[tile.id.y][tile.id.x + 1];
        neighbors.push(currentTile);
    }

    return neighbors;
};

// get tile walkable neighbors
Board.prototype.getWalkableNeighbors = function (tile, player) {
    var neighbors = [];
    var currentTile = null;

    //UP
    if (this.isWalkable(new Vector2(tile.id.x, tile.id.y - 1), player.id)) {
        currentTile = this.getTiles()[tile.id.y - 1][tile.id.x];
        neighbors.push(currentTile);
    }

    //DOWN
    if (this.isWalkable(new Vector2(tile.id.x, tile.id.y + 1), player.id)) {
        currentTile = this.getTiles()[tile.id.y + 1][tile.id.x];
        neighbors.push(currentTile);
    }

    //LEFT
    if (this.isWalkable(new Vector2(tile.id.x - 1, tile.id.y), player.id)) {
        currentTile = this.getTiles()[tile.id.y][tile.id.x - 1];
        neighbors.push(currentTile);
    }

    //RIGHT
    if (this.isWalkable(new Vector2(tile.id.x + 1, tile.id.y), player.id)) {
        currentTile = this.getTiles()[tile.id.y][tile.id.x + 1];
        neighbors.push(currentTile);
    }

    return neighbors;
};

// gets firt board first element
Board.prototype.getFirst = function () {
    return this.getTiles()[0][0];
};

// gets firt board last element
Board.prototype.getLast = function () {
    return this.getTiles()[this.getYSize() - 1][this.getXSize() - 1];
};

// checks if board includes tile
Board.prototype.includes = function (tile) {
    if (this.getTiles()[tile.id.y][tile.id.x]) {
        return true;
    }

    return false;
}
