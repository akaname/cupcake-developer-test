//
// AI PLOTTING
//


// 
// PATHFINDING
//

function Pathfinding(player) {
    this.player = player;
    this.path = [];
}

Pathfinding.prototype.initializeAStar = function (board) {
    for (var y = 0; y < board.getTiles().length; y++) {
        for (var x = 0; x < board.getTiles()[y].length; x++) {
            board.getTiles()[y][x].f = 0;
            board.getTiles()[y][x].g = 0;
            board.getTiles()[y][x].h = 0;
            board.getTiles()[y][x].tileParent = null;
            board.getTiles()[y][x].debug = "";
        }
    }
};

Pathfinding.prototype.aStar = function (board, startTile, endTile) {
    this.path = [];
    this.initializeAStar(board);

    var openSet = [];
    var closedSet = [];

    openSet.push(startTile);

    while (openSet.length > 0) {
        var lowestIndex = 0;

        for (var i = 0; i < openSet.length; i++) {
            if (openSet[i].f < openSet[lowestIndex].f) {
                lowestIndex = i;
            }
        }

        var currentTile = openSet[lowestIndex];

        if (currentTile.equals(endTile)) {
            var tile = currentTile;
            var path = [];

            while (tile.tileParent) {
                path.push(tile);
                tile = tile.tileParent;
            }

            this.path = path.reverse();
            return;
        }

        openSet = this.removeTile(openSet, currentTile);
        closedSet.push(currentTile);

        var neighbors = board.getWalkableNeighbors(currentTile, this.player);

        for (var i = 0; i < neighbors.length; i++) {
            var currentNeighbor = neighbors[i];

            if (board.hasTile(closedSet, currentNeighbor)) {
                continue;
            }

            var gValue = currentTile.g + 10;
            var gValueIsBest = false;

            if (!board.hasTile(openSet, currentNeighbor)) {
                currentNeighbor.h = this.heuristic(currentNeighbor, endTile);
                gValueIsBest = true;
                openSet.push(currentNeighbor);
            } else if (gValue < currentNeighbor.g) {
                gValueIsBest = true;
            }

            if (gValueIsBest) {
                currentNeighbor.tileParent = currentTile;
                currentNeighbor.g = gValue;
                currentNeighbor.f = currentNeighbor.g + currentNeighbor.h;
                //currentNeighbor.debug = currentNeighbor.f + '|' + currentNeighbor.g + '|' + currentNeighbor.h;
                //currentNeighbor.DEBUGTILE(currentNeighbor.debug);
            }
        }
    }
};

Pathfinding.prototype.heuristic = function (tile0, tile1) {
    var xDistance = Math.abs(tile1.id.x - tile0.id.x);
    var yDistance = Math.abs(tile1.id.y - tile0.id.y);
    return xDistance + yDistance;
};

Pathfinding.prototype.removeTile = function (array, tile) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].equals(tile)) {
            array.splice(i, 1);
            i--;
        }
    }
    return array;
};

Pathfinding.prototype.hasPath = function () {
    if (this.path.length > 0) {
        return true;
    }

    return false;
};

// 
// DEPTHFIRSTSEARCH
//

function DepthFirstSearch(player) {
    this.player = player;
    this.discoveredTiles = [];
}

DepthFirstSearch.prototype.complexMove = function (board, targetTile, complexity) {
    this.discoveredTiles = [];

    if (!board || !targetTile || !complexity) {
        return;
    }

    var steps = 0;
    var neighbors = board.getNeighbors(targetTile);

    while (steps < complexity) {
        var nextNeighbors = [];

        for (var i = 0; i < neighbors.length; i++) {
            var currentNeighbor = neighbors[i];

            if (!this.hasTile(currentNeighbor)) {
                var dfsTileNeighbors = board.getWalkableNeighbors(currentNeighbor, this.player);
                var dfsTile = new DFSTile(currentNeighbor, steps, dfsTileNeighbors.length, this.player.id);
                this.discoveredTiles.push(dfsTile);
            }

            var newNeightbors = board.getNeighbors(currentNeighbor);

            for (var j = 0; j < newNeightbors.length; j++) {
                if (!this.hasTile(newNeightbors[j]) &&
                    !newNeightbors[j].equals(targetTile) &&
                    !newNeightbors[j].equals(this.player.tile)) {
                    nextNeighbors.push(newNeightbors[j]);
                }
            }
        }

        neighbors = nextNeighbors;
        steps++;
    }

    this.filter(board);
};

DepthFirstSearch.prototype.simpleMove = function (board) {
    this.discoveredTiles = [];

    if (!board) {
        return;
    }

    var neighbors = board.getWalkableNeighbors(this.player.tile, this.player);
    var dsfTiles = []

    for (var i = 0; i < neighbors.length; i++) {
        var currentNeighbor = neighbors[i];
        var dfsTileNeighbors = board.getWalkableNeighbors(currentNeighbor, this.player);

        var dsfTile = new DFSTile(currentNeighbor, 0, dfsTileNeighbors.length, this.player.id);
        dsfTiles.push(dsfTile);
    }

    this.discoveredTiles = dsfTiles;
};

DepthFirstSearch.prototype.filter = function (board) {
    for (var i = 0; i < this.discoveredTiles.length; i++) {
        if (!board.isWalkable(this.discoveredTiles[i].tile.id, this.player.id)) {
            this.discoveredTiles.splice(i, 1);
            i--;
        }
    }
};

DepthFirstSearch.prototype.hasTile = function (tile) {
    for (var i = 0; i < this.discoveredTiles.length; i++) {
        if (this.discoveredTiles[i].tile.equals(tile)) {
            return true;
        }
    }
    return false;
};

DepthFirstSearch.prototype.sort = function () {
    this.discoveredTiles.sort(function (a, b) {
        return b.weight - a.weight;
    });
};

DepthFirstSearch.prototype.hasMove = function () {
    if (this.discoveredTiles.length > 0) {
        return true;
    }

    return false;
};

// 
// DEPTHFIRSTSEARCH TILE
//

function DFSTile(tile, distance, length, playerId) {
    this.tile = tile;

    var exposition = this.tile.isEnemy(playerId) ? 5 * (4 - length) : 2 * (4 - length);

    // weight: to balance the board, to reach each difficulty
    // distance: number of tiles away from the target
    // length: number of walkable tiles around
    // exposition: tendenssi to move towards enemy tiles
    this.weight = (distance * 5) + (length * 4) - exposition;
};
