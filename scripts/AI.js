//
// Game Artificial Intelligence
//

AI.prototype = Object.create(Player.prototype);
AI.prototype.constructor = AI;

AI.WAIT_TIME = 1000;

function AI(id, spriteTag, trailTag, name) {
    Player.call(this, id, spriteTag, trailTag, name);

    var maxTries = 0;
    var tries = 0;
    var complexity = 6;

    this.pathfinding = new Pathfinding(this);
    this.dfs = new DepthFirstSearch(this);
    this.wait = true;

    this.maxTries = function (max) {
        if (max) {
            maxTries = max;
        } else {
            return maxTries;
        }
    };

    this.tries = function (newTries) {
        if (newTries) {
            tries = newTries;
        } else {
            return tries;
        }
    };

    this.complexity = function (newComplexity) {
        if (newComplexity) {
            complexity = newComplexity;
        } else {
            return complexity;
        }
    };
}

AI.prototype.beginMove = function () {
    this.wait = true;

    setTimeout(AI.prototype.waitIsDone.bind(this), AI.WAIT_TIME);
};

AI.prototype.waitIsDone = function () {
    this.wait = false;
};

AI.prototype.move = function (turn) {
    if (this.wait) {
        return;
    }

    var hasMove = false;

    if (this.isMoveValid(this.pathfinding.path[0])) {
        var id = this.pathfinding.path.shift().id;
        var direction = this.getDirection(id);

        turn.setMove(id, this.id, direction);

        this.tries(this.tries() - 1);
    } else {
        this.dfs.complexMove(turn.board, turn.getOtherPlayer().tile, this.complexity());

        if (this.dfs.hasMove()) {
            var objective = this.getMove(true);

            var path = this.pathfinding.aStar(turn.board, this.tile, objective);

            if (this.pathfinding.hasPath()) {
                var id = this.pathfinding.path.shift().id;
                var direction = this.getDirection(id);

                turn.setMove(id, this.id, direction);

                this.tries(this.maxTries());
                hasMove = true;
            }
        }

        if (!hasMove) {
            this.dfs.simpleMove(turn.board);

            if (this.dfs.hasMove()) {
                var objective = this.getMove();
                var direction = this.getDirection(objective.id);

                turn.setMove(objective.id, this.id, direction);
            }
        }
    }
};

AI.prototype.getMove = function (complex = false) {
    if (complex) {
        this.dfs.sort();

        return this.dfs.discoveredTiles[0].tile;
    } else {
        this.dfs.sort();

        return this.dfs.discoveredTiles[0].tile;
    }

    return null;
};

AI.prototype.animation = function (delta) {
    this.blink(delta);
};

AI.prototype.isMoveValid = function (tile) {
    if (this.tries() > 0 && tile) {
        if (tile.isWalkable(this.id)) {
            return true;
        }
    }

    return false;
};

AI.prototype.getDirection = function (move) {
    if (move.y < this.tile.id.y) {
        return Directions.UP;
    } else if (move.y > this.tile.id.y) {
        return Directions.DOWN;
    } else if (move.x < this.tile.id.x) {
        return Directions.LEFT;
    } else if (move.x > this.tile.id.x) {
        return Directions.RIGHT;
    }

    return undefined;
};
//
// EASY AI
//

AI_Easy.prototype = Object.create(AI.prototype);
AI_Easy.prototype.constructor = AI_Easy;

function AI_Easy(id, color, spriteTag, trailTag, name) {
    AI.call(this, id, color, spriteTag, trailTag, name);
    this.maxTries(5);
    this.complexity(3);
}

AI_Easy.prototype.getMove = function (complex = false) {
    var lastIndex = this.dfs.discoveredTiles.length - 1;

    if (complex) {
        this.dfs.sort();

        if (this.dfs.discoveredTiles.length > 2) {
            var min = this.dfs.discoveredTiles[lastIndex].weight;
            var max = this.dfs.discoveredTiles[0].weight;
            var med = (min + max) / 2;

            var opstions = [];

            for (var i = 0; i < this.dfs.discoveredTiles.length; i++) {
                if (this.dfs.discoveredTiles[i].weight < med) {
                    opstions.push(this.dfs.discoveredTiles[i].tile);
                }
            }

            var randomOption = Math.floor(Math.random() * opstions.length);
            return opstions[randomOption];

        } else {
            return this.dfs.discoveredTiles[lastIndex].tile;
        }
    } else {
        this.dfs.sort();
        return this.dfs.discoveredTiles[lastIndex].tile;
    }

    return null;
};

//
// MEADIUM AI
//

AI_Medium.prototype = Object.create(AI.prototype);
AI_Medium.prototype.constructor = AI_Medium;

function AI_Medium(id, color, spriteTag, trailTag, name) {
    AI.call(this, id, color, spriteTag, trailTag, name);
    this.maxTries(2);
    this.complexity(5);
}

AI_Medium.prototype.getMove = function (complex = false) {
    var lastIndex = this.dfs.discoveredTiles.length - 1;

    if (complex) {
        this.dfs.sort();

        if (this.dfs.discoveredTiles.length > 2) {
            var min = this.dfs.discoveredTiles[lastIndex].weight;
            var max = this.dfs.discoveredTiles[0].weight;
            var med = (min + max) / 2;

            var opstions = [];

            for (var i = 0; i < this.dfs.discoveredTiles.length; i++) {
                if (this.dfs.discoveredTiles[i].weight >= med &&
                    this.dfs.discoveredTiles[i].weight < max) {
                    opstions.push(this.dfs.discoveredTiles[i].tile);
                }
            }

            var randomOption = Math.floor(Math.random() * opstions.length);
            return opstions[randomOption];

        } else {
            return this.dfs.discoveredTiles[lastIndex].tile;
        }
    } else {
        this.dfs.sort();
        return this.dfs.discoveredTiles[lastIndex].tile;
    }

    return null;
};
