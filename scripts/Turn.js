//
// Handle turns logic
//

function Turn(ticker, board, players, finishGameCallback) {
    this.ticker = ticker;
    this.board = board;
    this.players = players;
    this.finishGame = finishGameCallback;
    this.move = null;
    this.direction = null;
    this.currentPlayer = Math.floor(Math.random() * 2);
}

// begins turn actions
Turn.prototype.play = function () {
    this.getCurrentPlayer().beginMove(this);
};

// updates turn
Turn.prototype.update = function () {
    this.getCurrentPlayer().move(this);

    if (this.move != null) {
        if (this.board.isWalkable(this.move, this.getCurrentPlayer().id)) {
            this.getCurrentPlayer().setTile(
                this.board.getTiles()[this.move.y][this.move.x],
                this.direction
            );
            this.next();
        }
        this.consume();
    }
};

// updates animations
Turn.prototype.animations = function (delta) {
    this.getCurrentPlayer().animation(delta);
};

// receives and checks players move
Turn.prototype.setMove = function (move, id, direction) {
    if (id === this.getCurrentPlayer().id) {
        if (this.canMove(move, this.getCurrentPlayer().tile)) {
            this.move = move;
            this.direction = direction;
        }
    }
};

// checks if move is available
Turn.prototype.canMove = function (move, playerTile) {
    if (!move || !playerTile) {
        return false;
    }

    var xDiff = Math.abs(move.x - playerTile.id.x);
    var yDiff = Math.abs(move.y - playerTile.id.y);

    if (xDiff <= 1 && yDiff <= 1) {
        return true;
    }

    return false;
};

// consumes current move
Turn.prototype.consume = function () {
    this.move = null;
    this.direction = null;
};

// goes to next turn and handles losses
Turn.prototype.next = function () {
    this.getCurrentPlayer().endMove(this);

    if (!this.board.hasAvailableMove(this.getCurrentPlayer())) {
        this.afterGame();
        return;
    }

    this.currentPlayer = (this.currentPlayer + 1) % this.players.length;

    if (!this.board.hasAvailableMove(this.getCurrentPlayer())) {
        this.afterGame();
        return;
    }

    this.getCurrentPlayer().beginMove(this);
};

// starts player losing animation
Turn.prototype.afterGame = function () {
    this.getCurrentPlayer().startDisappearance(this.ticker, this.end.bind(this));
};

// ends all turns
Turn.prototype.end = function () {
    this.getOtherPlayer().win();
    this.finishGame(this.getOtherPlayer());
};

// getter for current player
Turn.prototype.getCurrentPlayer = function () {
    return this.players[this.currentPlayer];
};

// getter for the other player
Turn.prototype.getOtherPlayer = function () {
    var other = (this.currentPlayer + 1) % this.players.length;
    return this.players[other];
};

// resets all turns
Turn.prototype.reset = function (stage) {
    for (var i = 0; i < this.players.length; i++) {
        stage.removeChild(this.players[i]);
    }

    this.players = undefined;
};
