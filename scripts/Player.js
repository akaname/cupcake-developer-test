//
// Handles players, inherits from PIXI Sprite
//

// sprite tags
Player.CAT_TAG = 'cat';
Player.DOG_TAG = 'dog';
Player.CAT_PAW_TAG = 'catpaw';
Player.DOG_PAW_TAG = 'dogpaw';

// inheritance
Player.prototype = Object.create(PIXI.Sprite.prototype);
Player.prototype.constructor = Player;

function Player(id, spriteTag, trailTag, name) {
    var texture = new PIXI.Texture.fromImage(spriteTag);
    PIXI.Sprite.call(this, texture);

    // player properties
    this.id = id;
    this.trailTag = trailTag;
    this.team = name;
    this.alphaValue = 1;
    this.toOne = false;
    this.team = name;

    // sprite properties
    this.width = 64;
    this.height = 64;
    this.anchor.set(0.5);
}

// tile/position in the board 
Player.prototype.setTile = function (tile, direction, playSound = true) {
    if (this.tile != null) {
        this.tile.unoccupy();
    }

    this.tile = tile;
    this.position.x = tile.worldTransform.tx;
    this.position.y = tile.worldTransform.ty;
    this.tile.tint = this.tint;
    this.tile.addTrail(this.trailTag, direction);
    this.tile.occupy(this.id);

    if (this.audioMove && playSound) {
        this.audioMove.play()
    }
};

// sets player audio
Player.prototype.setAudio = function (move, win, loose) {
    this.audioMove = move;
    this.audioWin = win;
    this.audioLoose = loose;
};

// player position in the board
Player.prototype.getID = function () {
    return new Vector2(this.tile.id.x, this.tile.id.y);
};

// make move
Player.prototype.move = function (turn) {};

// starts move
Player.prototype.beginMove = function (turn) {};

// ends move
Player.prototype.endMove = function (turn) {
    this.alpha = 1;
    this.alphaValue = 1;
    this.toOne = false;
};

// animation calls
Player.prototype.animation = function (delta) {
    this.blink(delta);
};

// blink animation
Player.prototype.blink = function (delta) {
    var amount = delta * 0.05;
    var newAlpha = this.toOne ? (this.alphaValue + amount) : (this.alphaValue - amount);
    this.alphaValue = Math.lerp(this.alphaValue, newAlpha, 0.5);
    this.alpha = this.alphaValue;

    if (this.alphaValue >= 1) {
        this.toOne = false;
    } else if (this.alphaValue <= 0.2) {
        this.toOne = true;
    }
};

// starts disappear function
Player.prototype.startDisappearance = function (ticker, callback) {
    ticker.add(this.disappear, this);
    this.afterDisappearance = callback;
    this.ticker = ticker;

    if (this.audioLoose) {
        this.audioLoose.play();
    }
};

// fade plus rotation when player lost
Player.prototype.disappear = function (delta) {
    this.width -= delta * 0.5;
    this.height -= delta * 0.5;
    this.rotation += delta * 0.15;

    if (this.width <= 0.1 || this.height <= 0.1) {
        this.width = 0;
        this.height = 0;

        this.ticker.remove(this.disappear, this);
        this.afterDisappearance();
    }
};

// play win audio
Player.prototype.win = function () {
    if (this.audioWin) {
        this.audioWin.play();
    }
};
