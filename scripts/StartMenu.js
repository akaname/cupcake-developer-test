//
// StartMenu contains players picks and game title
//

StartMenu.AUDIO_FLIP = 'flip';
StartMenu.AUDIO_MEOW = 'meow';
StartMenu.AUDIO_BARK = 'bark';

function StartMenu(screen, ticker, audio) {
    this.ticker = ticker;
    this.startMenuArea = new PIXI.Container();
    this.screenWidth = screen.width;
    this.screenHeight = screen.height;
    this.title = null;
    this.selects = [];
    this.currentSelect = 0;
    this.selectPlayer = null;
    this.createGame = undefined;
    this.player1 = null;
    this.player2 = null;
    this.flip = null;
}

StartMenu.prototype.initialize = function (stage, audio, createGame) {
    stage.addChild(this.startMenuArea);

    this.flip = audio.flip;

    this.createGame = createGame;

    this.title = new StartMenuTitle();
    this.title.addToCanvas(this.startMenuArea);

    var select1 = new SelectPlayer('SELECT CAT', Player.CAT_TAG, 0, -200, Keys.SET.WASD, this.setPlayer1.bind(this));
    select1.addToCanvas(this.startMenuArea);
    select1.setAudio(audio.meow);

    var select2 = new SelectPlayer('SELECT DOG', Player.DOG_TAG, 20, 200, Keys.SET.ARROWS, this.setPlayer2.bind(this));
    select2.addToCanvas(this.startMenuArea);
    select2.setAudio(audio.bark);

    this.selects.push(select1, select2);

    this.startMenuArea.position.x = this.screenWidth / 2;
    this.startMenuArea.position.y = this.screenHeight / 2;

    this.selectPlayer = new Confirm();

    this.registerKeys();
    this.selection().open();

    this.ticker.add(this.update, this);
};

StartMenu.prototype.update = function (delta) {
    if (this.selection()) {
        this.selection().blink(delta);
    }
};

StartMenu.prototype.leaveScreen = function (delta) {
    this.startMenuArea.position.x += delta * 10;

    if (this.startMenuArea.x > this.screenWidth * 2) {
        this.ticker.remove(this.leaveScreen, this);
        this.createGame(this.player1, this.player2);
    }
};

StartMenu.prototype.confirmPlayer = function () {
    this.selection().close();
    this.currentSelect++;

    if (this.currentSelect < this.selects.length) {
        this.registerKeys();
        this.selection().open();

    } else {
        this.selectPlayer.unregister();
        this.ticker.remove(this.update, this);
        this.ticker.add(this.leaveScreen, this);
    }
};

StartMenu.prototype.selection = function () {
    return this.selects[this.currentSelect];
};

StartMenu.prototype.registerKeys = function () {
    this.selectPlayer.confirm.pressed = () => {
        this.confirmPlayer();
    };

    this.selection().keys.up.pressed = () => {
        this.flip.play();
        this.selection().switchOption(-1);
    };

    this.selection().keys.down.pressed = () => {
        this.flip.play();
        this.selection().switchOption(1);
    };
};

StartMenu.prototype.setPlayer1 = function (type) {
    this.player1 = type;
};

StartMenu.prototype.setPlayer2 = function (type) {
    this.player2 = type;
};

StartMenu.prototype.reset = function () {
    this.currentSelect = 0;
    this.startMenuArea.position.x = this.screenWidth * 0.5;
    this.player1 = null;
    this.player2 = null;

    for (var i = 0; i < this.selects.length; i++) {
        this.selects[i].setAlpha(SelectPlayer.HIDE_ALPHA);
        this.selects[i].keysImage.visible = false;
    }
};

StartMenu.prototype.startAfterReset = function () {
    this.registerKeys();
    this.selection().open();
    this.ticker.add(this.update, this);
};

StartMenuTitle.prototype.addToCanvas = function (container) {
    container.addChild(this.font);
};

//
// Game Title: Cupcake Dev Test
//

function StartMenuTitle() {
    this.text = 'Cupcake\nDev\nTest';
    this.font = new PIXI.extras.BitmapText(this.text, {
        font: "32px font",
        align: "center",
    });
    this.font.anchor.set(0.5, 0.5);
    this.font.position.y = -150;
}

//
// SelectPlayer is the player picking: cat and dog
//

// Player selection types
SelectPlayer.TYPE = {
    HUMAN: 0,
    PC_EASY: 1,
    PC_MED: 2,
    PC_HARD: 3
}

//SelectPlayer Alpha sets
SelectPlayer.HIDE_ALPHA = 0.65;
SelectPlayer.SHOW_ALPHA = 1;

function SelectPlayer(text, spriteTag, spriteOffsetY, xposition, keySet, player) {
    this.options = [];
    this.currentOption = 0;
    this.alphaValue = 1;
    this.toOne = false;
    this.isSelected = false;
    this.keys = new Keys(keySet);
    this.player = player;
    this.selectAudio = null;

    // player sprite
    this.sprite = PIXI.Sprite.fromImage(spriteTag);
    this.sprite.anchor.set(0.5, 0.5);
    this.sprite.position.x = xposition;
    this.sprite.position.y = -140 + spriteOffsetY;

    // title
    this.text = text;
    this.titleFont = new PIXI.extras.BitmapText(this.text, {
        font: "22px font",
        align: "center",
    });
    this.titleFont.anchor.set(0.5, 0.5);
    this.titleFont.position.set(xposition, -50);

    //human player + keyboard
    var human = new PIXI.extras.BitmapText('PLAYER', {
        font: "18px font",
        align: "left",
    });
    human.anchor.set(0, 0.5);
    human.position.set(-50, 45);
    human.type = 0;

    // artificial intelligence easy
    var pcEasy = new PIXI.extras.BitmapText('PC Easy', {
        font: "18px font",
        align: "left",
    });
    pcEasy.anchor.set(0, 0.5);
    pcEasy.position.set(-50, 90);
    pcEasy.type = 1;

    // artificial intelligence medium
    var pcMedium = new PIXI.extras.BitmapText('PC Medium', {
        font: "18px font",
        align: "left",
    });
    pcMedium.anchor.set(0, 0.5);
    pcMedium.position.set(-50, 135);
    pcMedium.type = 2;

    // artificial intelligence hard
    var pcHard = new PIXI.extras.BitmapText('PC Hard', {
        font: "18px font",
        align: "left",
    });
    pcHard.anchor.set(0, 0.5);
    pcHard.position.set(-50, 180);
    pcHard.type = 3;

    this.keysImage = keySet === Keys.SET.WASD ?
        new PIXI.Sprite.fromImage(Keys.WASD_SPRITE) :
        new PIXI.Sprite.fromImage(Keys.ARROW_SPRITE);
    this.keysImage.anchor.set(0.5, 0.5);
    this.keysImage.scale.set(0.5, 0.5);
    this.keysImage.position.set(xposition, 210);
    this.keysImage.visible = false;

    this.options.push(human, pcEasy, pcMedium, pcHard);

    // confirmation idicator
    this.enter = new PIXI.extras.BitmapText('[enter]', {
        font: "18px font",
        align: "right",
    });
    this.enter.anchor.set(1, 0.5);
    this.enter.startPosition = new PIXI.Point(human.position.x - 10, human.position.y);
    this.enter.position = this.enter.startPosition;
    this.enter.visible = false;

    this.setAlpha(SelectPlayer.HIDE_ALPHA);
}

SelectPlayer.prototype.setAudio = function (audio) {
    this.selectAudio = audio;
};

SelectPlayer.prototype.addToCanvas = function (container) {
    container.addChild(this.sprite);
    container.addChild(this.titleFont);
    container.addChild(this.keysImage);

    for (var i = 0; i < this.options.length; i++) {
        this.titleFont.addChild(this.options[i]);
    }

    this.titleFont.addChild(this.enter);
};

SelectPlayer.prototype.open = function () {
    this.setAlpha(SelectPlayer.SHOW_ALPHA);
    this.enter.position = this.enter.startPosition;
    this.enter.visible = true;
    this.currentOption = 0;
    this.keysImage.visible = true;
};

SelectPlayer.prototype.close = function () {
    this.selectAudio.play();
    this.keys.unregister();
    this.setAlpha(SelectPlayer.HIDE_ALPHA);
    this.enter.visible = false;

    this.titleFont.alpha = 1;
    this.sprite.alpha = 1;
    this.options[this.currentOption].alpha = 1;
    this.player(this.options[this.currentOption].type);
};

SelectPlayer.prototype.setAlpha = function (value) {
    this.titleFont.alpha = value;
    this.sprite.alpha = value;

    for (var i = 0; i < this.options.length; i++) {
        this.options[i].alpha = value;
    }

    this.alphaValue = value;
};

SelectPlayer.prototype.blink = function (delta) {
    var amount = delta * 0.045;
    var newAlpha = this.toOne ? (this.alphaValue + amount) : (this.alphaValue - amount);
    this.alphaValue = Math.lerp(this.alphaValue, newAlpha, 0.5);

    this.options[this.currentOption].alpha = this.alphaValue;
    this.enter.alpha = this.alphaValue;

    if (this.alphaValue >= 1) {
        this.toOne = false;
    } else if (this.alphaValue <= 0.4) {
        this.toOne = true;
    }
};

SelectPlayer.prototype.switchOption = function (value) {
    this.options[this.currentOption].alpha = 1;

    this.currentOption = (this.currentOption + value) % this.options.length;

    if (this.currentOption < 0) {
        this.currentOption = this.options.length - 1;
    }

    if (this.options[this.currentOption].type === SelectPlayer.TYPE.HUMAN) {
        this.keysImage.visible = true;
    } else {
        this.keysImage.visible = false;
    }

    this.enter.position.y = this.options[this.currentOption].position.y;
};
