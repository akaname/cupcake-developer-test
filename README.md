$ HOW TO RUN GAME

1. Unzip files or clone the repo
2. Setup local server in the root folder at port 3000
3. Open your browser and go to localhost:3000
4. Play the game*

* game tested in:
Mozilla Firefox 58.0.2 (64-bit)
Google Chrome 64.0.3282.167 (64-bit)

##############################################################################################################


$ HOW TO SETUP LOCAL SERVER
 
1. check if python is installed, 
    in your terminal type: python -v
  
    a) if it does not return a version number, then go to python.org, download and install python 3.xxx
    
2. open the termial and navigate to the project root directory
3. type: python -m http.server 3000
4. your local server is ready!


##############################################################################################################


$ HOW TO OPEN PROJECT

1. Go to the project folder
2. Right click and open folder in your editor (e.g. Brackets: right-click->Open as Brackets project)
3. Look around


##############################################################################################################


THANKS!